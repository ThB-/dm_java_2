import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
     public static Integer plus(Integer a, Integer b){
         return a+b;
     }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
     public static Integer min(List<Integer> liste){
         Integer min = null;
         for (Integer elem : liste)
         {
             if (min == null)
                 min = elem;
             else
                 if (elem < min)
                 {
                     min = elem;
                 }
         }
         return min;
     }



    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
     public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T elem , List<T> liste){
         boolean petit = true;
         int i = 0;
         while (petit && i < liste.size())
         {
             if (elem.compareTo(liste.get(i)) >= 0)
                 petit = false;
             i++;
         }
         return petit;
     }




    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
     public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
         List<T> listeInter = new ArrayList<>();
         int i = 0;
         int y = 0;
         while (i < liste1.size() && y < liste2.size()){ // Tant que qu'on peut comparer dexu élements de la liste
             if (liste1.get(i).equals(liste2.get(y)))
             {
               if(!listeInter.contains(liste1.get(i))){
                 listeInter.add(liste1.get(i));} // S'ils sont égaux, on les ajoute dans la liste.
                 i++;
                 y++;
               }
          else{
            if (liste1.get(i).compareTo(liste2.get(y)) > 0)
            {y++;}
            else
            {i++;}
          }
          }
          return listeInter;
        } //On renvoie les éléments en communs des deux listes liste1 et liste2




    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
     public static List<String> decoupe(String text){
         List<String> listemots = new ArrayList<>();
         String[] mots = text.split(" ");
         for (int i=0; i<mots.length; i++)
             if (!mots[i].equals("")) // Si le caractère n'est pas un caractère vide, alors on ajoute la lettre dans la liste de mots.
                 listemots.add(mots[i]);
         return listemots;
     }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

      public static String motMajoritaire(String texte) {
      List<String> listeMots = decoupe(texte);
      Hashtable<String, Integer> repetition = new Hashtable<>();
      for (String mot : listeMots) {
          if (repetition.contains(mot))
              repetition.put(mot, repetition.get(mot) + 1);
          else
              repetition.put(mot, 1);
      }
      String motMax = null;
      for (String mot : repetition.keySet()) {
          if (motMax == null || repetition.get(mot) > repetition.get(motMax))
              motMax = mot;
          else if (repetition.get(mot) == repetition.get(motMax))
              if (mot.compareTo(motMax) < 0)
                  motMax = mot;
      }
      return motMax;
  }




    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
     public static boolean bienParenthesee(String chaine){
         int nbOuvrantes = 0;
         int nbFermantes = 0;
         String[] carac = chaine.split(""); // Je m'aide en décomposant la chaîne sous forme d'index me donnant des "char"
         for (int i=0; i<carac.length; i++)
         {
             if (nbFermantes <= nbOuvrantes)
              if(nbOuvrantes !=0)
                 if (carac[i].equals(")"))
                     nbFermantes++;
             if (carac[i].equals("("))
                 nbOuvrantes++;
         }
         return (nbFermantes == nbOuvrantes && !carac[carac.length-1].equals("("));
     }




    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
     public static boolean bienParentheseeCrochets(String chaine){
      List<Character> caracs = new ArrayList<>();
      if(chaine.length()>0){
      for( int i = 0 ; i < chaine.length(); i++){
        if (chaine.charAt(i) == '('){
             caracs.add('(');}
         if (chaine.charAt(i) == ')'  && caracs.size()>0 && caracs.get(caracs.size() - 1) == '(' ){
           caracs.remove(caracs.size()-1);}
         else{
           return false;}
         if (chaine.charAt(i) == '['){

           caracs.add('[');
         }
         if (chaine.charAt(i) == ']'  && caracs.size()>0 && caracs.get(caracs.size() - 1) == '['){
           caracs.remove(caracs.size()-1);}
         else{
           return false;
         }
       }
     }
     return caracs.size() == 0;
     }







    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
     public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur) {
           int fin = liste.size();
           int debut = 0;

           if (liste.size()> 0) {
           while (debut < fin) {
               int milieu = ((debut + fin) / 2);
               if (liste.get(milieu).compareTo(valeur) < 0) {
                   debut = milieu +1;
               }

               else{
                 fin = milieu;
               }
           }
       }
       return (debut < liste.size() && valeur.equals(liste.get(debut)));

   }





}
